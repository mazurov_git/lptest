$(document).ready(function () {

});

$('a[href^="#"]').on('click', function (event) {

    var target = $(this.getAttribute('href'));

    if (target.length) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});


// ===== Scroll to Top ==== 
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function () {      // When arrow is clicked
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
});


// ===== calculator ==== 
var px = document.querySelector('.px-input');
var em = document.querySelector('.em-input');
var bf = document.querySelector('.chosen');
var pxValue = px.value;
var basefont = bf.value;
px.addEventListener('keyup', function () {
    basefont = bf.value;
    pxValue = px.value;
    calculatePXtoEM(pxValue, basefont);
});

em.addEventListener('keyup', function () {
    basefont = bf.value;
    emValue = em.value;
    calculateEMtoPX(emValue, basefont);
});

function calculatePXtoEM(givenPx, basefont) {
    console.log(givenPx);
    if (basefont === '') {
        basefont = 5.74;
    }
    var returnEm = (givenPx * basefont);
    // em.value = "â‚¬" + returnEm.toFixed(1);
    em.value = returnEm.toFixed(1);
}

function calculateEMtoPX(givenEm, basefont) {
    console.log(givenEm);
    if (basefont === '') {
        basefont = 5.74;
    }
    var returnPx = (givenEm * basefont);
    // px.value = "â‚¬" + returnPx.toFixed(1);
    px.value = returnPx.toFixed(1);
}


//bitcoin price getJSON
$(function () {
    function getBPI() {
        var jsonURL = 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=BTC,USD,EUR';
        $.getJSON(jsonURL,
            function (result) {
                var oldPrice = 966.6;
                var rate = result.USD;
                var percent = Math.round(((rate / oldPrice) * 100) - 1 * 100);

                $('.bpi').html(rate);
                $('.percent').html(percent);
                $('.calcPercent').val(percent / 100);
                // document.title = rate;
                // num.toFixed(0);
            });
    }

    var timer = setInterval(getBPI, 60000);
    getBPI();
});

