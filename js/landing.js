$.fn.andSelf = function() {
    return this.addBack.apply(this, arguments);
}
function getCountries(countryIso){
    let countries = {};
    let prefix = null

    $.ajax({
        url: window.location.origin + '/countries_local.json',
        success: function(data) {
            countries = data;
            prefix = '+' + countries[countryIso].dial_code;
        },
        async:false
    });

    return prefix;
}

function getCountryByIpCustom() {
    let country = null;
    let prefix = null;
    let data = {};
    $.ajax({
        type: "GET",
        url: '//iplocation.genie-solution.com/iplocation',
        dataType: "json",
        async: false,
        success: function (response) {
            country = response && response.country ? response.country : null;
            prefix = getCountries(country)
        },
        error: function (error) {
            console.log('error: ', error)
        }
    });

    data.country = country
    data.prefix = prefix
    return data
}

function customCardrotate() {
    function run() {
        $('.card__item').each(function () {
            $(this).toggleClass('active')
        })
    }
    var intervalTimerRun= setInterval(run, 3000);
}

$(document).ready(function(){
    var geo_data = getCountryByIpCustom();

    if($('#BirthDate').length > 0) {
        $('#BirthDate').mask("00/00/0000", {placeholder: "تاريخ الميلاد"})
    }

    $('input#Country').val(geo_data.country);
    $('input#PhoneCountryCode').val(geo_data.prefix);

    $("a[data-nav-section]").click(function(e) {
        e.preventDefault();
        let scrollToName = $(this).data()['navSection'];
        if(!!scrollToName) {
            $('html, body').animate({
                scrollTop: $('[data-section=' + scrollToName + ']').offset().top - 66
            }, 500);
        }
    });

    if($('.card__item') && $('.card__item').length > 0 && $( window ).width() > 991) {
        customCardrotate();
    }
});


$('a[href^="#"]').on('click', function(event) {

    var target = $(this.getAttribute('href'));

    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }

});


// ===== Scroll to Top ====
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});






$(document).ready(function(){
    if($("#testimonial-slider") && $("#testimonial-slider").length > 0) {
        $("#testimonial-slider").owlCarousel({
            items:3,
            itemsDesktop:[1000,2],
            itemsDesktopSmall:[979,2],
            itemsTablet:[768,2],
            itemsMobile:[650,1],
            pagination:false,
            navigation:true,
            navigationText:["",""],
            autoPlay:true
        });
    }
});

