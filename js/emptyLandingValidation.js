var isSubmit = false;

function convertUrlVarsToJson(url) {
    var hash;
    var myJson = {};
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        myJson[hash[0]] = decodeURIComponent(hash[1].replace(/\+/g, ' '));
    }
    return myJson;
}

var loader = {
    isLoaderExist: function isLoaderExist() {
        return !!$('.laoder') && $('.laoder').length > 0;
    },
    show: function show() {
        if (this.isLoaderExist) {
            $('.loader').fadeIn('slow');
        }
    },
    hide: function hide() {
        if (this.isLoaderExist) {
            $('.loader').fadeOut('slow');
        }
    }
};

function submitActionForm(e) {
    let form = $(e.target)
    if (!isLandingValid(form)) {
        isSubmit = false;
    }
    else if (!isSubmit) {
        loader.show();
        form.find('button[type=submit]').attr('disabled', true);
        var the_api_url = window.location.origin + '/landing_helpers/action.php';
        var serialized_data = form.serialize();
        var form_data = convertUrlVarsToJson(serialized_data);
        var succes_page_url = window.location.origin + window.location.pathname + 'thank-you';
        // form_data.terms = $('#Terms').is(":checked");
        $.each($('.error'), function (item) {
            $(this).hide();
        });
        $.ajax({
            type: "POST",
            dataType: "json",
            url: the_api_url,
            data: form_data,
            success: function (response) {
                loader.hide();
                console.log(response)
                form.find('button[type=submit]').attr('disabled', false);
                if (response.status === 'error') {
                    if (response.message) {
                        let error_message = response.message;
                        if (response.api_campaign === 'koch' || response.api_campaign === 'triland-metals') {
                            switch (response.message) {
                                case 'invalidFirstName' :
                                    error_message = 'Prénom invalide';
                                    break;
                                case 'invalidLastName' :
                                    error_message = 'Nom invalide';
                                    break;
                                case 'invalidEmail' :
                                    error_message = 'Email invalide';
                                    break;
                                case 'invalidPhoneNo' :
                                    error_message = 'Téléphone invalide';
                                    break;
                                case 'invalidCountry' :
                                    error_message = 'Pays invalide';
                                    break;
                            }
                        }
                        form.find('.formError').text(error_message).show();
                    }
                } else {
                    //set redirection page as auto-login url for cryptoknights platform
                    if (response.api_campaign === 'kryptoknights') {
                        if (response.result && response.result.brokerLoginUrl) {
                            succes_page_url = response.result.brokerLoginUrl;
                        }
                    }
                    window.location.href = succes_page_url
                }
            },
            error: function (response_error) {
                $('.formError').text('Une erreur s\'est produite. Veuillez réessayer').show();
                form.find('button[type=submit]').attr('disabled', false);
                loader.hide();
                console.log('ajax error', response_error)
            }
        });
    }
}

function isLandingValid(form) {
    var isValid = true;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var firstName = form.find('input[name="FirstName"]');
    var lastName = form.find('input[name="LastName"]');
    var email = form.find('input[name="Email"]');
    var phoneNumber = form.find('input[name="PhoneNumber"]');
    var phoneCountryCode = form.find('input[name="PhoneCountryCode"]');
    var country = form.find('select[name="Country"]');
    var terms = form.find('input[name="Terms"]');

    if (!firstName.val()) {
        firstName.addClass('input-validation-error');
        firstName.siblings('.error').show().text('First name invalid');
        isValid = false;
    } else {
        if (firstName.val().length < 3) {
            firstName.addClass('input-validation-error');
            firstName.siblings('.error').show().text('First name invalid');
            isValid = false;
        }
    }
    if (!lastName.val()) {
        lastName.addClass('input-validation-error');
        lastName.siblings('.error').show().text('Last name invalid');
        isValid = false;
    }
    else {
        if (lastName.val().length < 3) {
            lastName.addClass('input-validation-error');
            lastName.siblings('.error').show().text('Last name invalid');
            isValid = false;
        }
    }

    if (!!firstName.val() && !!lastName.val() && firstName.val() == lastName.val()) {
        lastName.siblings('.error').show().text('First and Last name couldn\'t be the same.');
        isValid = false;
    }

    if (!email.val()) {
        email.val(email.attr("title"));
        email.addClass('input-validation-error');
        email.siblings('.error').show().text('Email invalid')
        isValid = false;
    } else {
        if (!emailReg.test(email.val())) {
            email.addClass('input-validation-error');
            email.siblings('.error').show().text('Email invalid');
            isValid = false;
        }
    }
    if (!phoneNumber.val()) {
        phoneNumber.val(phoneNumber.attr("title"));
        phoneNumber.addClass('input-validation-error');
        phoneNumber.siblings('.error').show().text('Phone invalid');
        isValid = false;
    }

    if (!country.val()) {
        country.parent().addClass('input-validation-error');
        isValid = false;
    }

    if (!phoneCountryCode.val()) {
        phoneCountryCode.addClass('input-validation-error');
        isValid = false;
    }

    var blockednum = ["111111", "222222", "333333", "444444", "555555", "666666", "777777", "888888", "999999", "000000", "123456", "123456789", "1234567", "12345678", "1234567890", "100000", "123123", "12341234", "21212121", "12121212"];
    if (!!phoneNumber.val()) {
        if (phoneNumber.val().length < 6 || $.inArray(phoneNumber.val(), blockednum) != -1) {
            phoneNumber.addClass('input-validation-error');
            phoneNumber.siblings('.error').show().text('Phone invalid');
            isValid = false;
        } else {
            phoneNumber.siblings('.error').hide()
        }
    }

    if (!terms.is(":checked")) {
        console.log('Terms: ', terms.val())
        terms.parent('.terms').siblings('.error').text('Please accept our terms and conditions.').show();
        isValid = false;
    } else {
        terms.parent('.terms-label').siblings('.error').hide();
    }

    return isValid;
}

$(document).ready(function () {
    $("input[name=PhoneCountryCode]").keydown(function (e) {
        AllowOnlyDigits(e);
    });
    $("input[name=PhoneNumber]").keydown(function (e) {
        AllowOnlyDigits(e);
    });
    $('select, input').change(function () {
        $(this).parent().removeClass('input-validation-error');
        $(this).siblings('.error').hide()
        $(this).removeClass('input-validation-error');
    });
    $('#Email').change(function () {
        $("#ErrorEmail").hide();
    });
    $("input[type='text']").keypress(function (e) {
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        InputValueWasChanged(e, str, this.name);
    });

    $("input[type='text']").bind("paste", function (e) {
        var str = e.originalEvent.clipboardData.getData('Text');
        InputValueWasChanged(e, str, this.name);
    });
});

function AllowOnlyDigits(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function InputValueWasChanged(e, str, name) {
    var nameIsFirstName = name.toLowerCase().includes("firstname");
    var nameIsLastName = name.toLowerCase().includes("lastname");

    var regex = new RegExp(nameIsFirstName || nameIsLastName ? "^[a-zA-Z\-\\s[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]-]+$" :
        "^[a-zA-Z0-9@:.,_#$%!*~?\-\\s[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]-]+$");
    if (regex.test(str)) {
        if (nameIsFirstName) {
            $("#ErrorFirstNameLength").hide();
            $("#ErrorNotMatch").hide();
        }
        if (nameIsLastName) {
            $("#ErrorLastNameLength").hide();
            $("#ErrorNotMatch").hide();
        }
        return true;
    }
    e.preventDefault();
    return false;
}

$(document).ready(function () {
    $('.actionForm').on('submit', function (e) {
        e.preventDefault()
        submitActionForm(e)
    });
})